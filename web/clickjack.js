// Taken from example pages by ZaphodFacets - Author Dr. Thomas Austin
var redirect = function(url) {
  document.location = url;
};

var addClickjacker = function(f) {
  document.addEventListener('click', f);
};


var newurl = "http://www.rennes.supelec.fr/ren/rd/cidre/?content=people";
addClickjacker(function() { redirect(newurl); });

