/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DatabaseConnector;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.WriteConcern;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.DBCursor;
import com.mongodb.ServerAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author deepak
 */
public class MongoConnector 
{
    public static MongoConnector mongoConnector = new MongoConnector();
    MongoClient mongoClient;
    DB db;
    DBCollection commentsCollection;
    protected MongoConnector()
    {
        try
        {
            this.mongoClient = new MongoClient();
            db = mongoClient.getDB( "seccloud" );
            commentsCollection = db.getCollection("comments");
        }
        catch (UnknownHostException unknownHEx)
        {
           Logger.getLogger(MongoConnector.class.getName()).log(Level.SEVERE, "MongoDB host not found", unknownHEx);
        }    
        catch (Exception ex)
        {
           Logger.getLogger(MongoConnector.class.getName()).log(Level.SEVERE, "Something wrong", ex);
        }  
    }
    public long getCollectionCount()
    {
        long count = 0 ;
   
        try
        {
           count = commentsCollection.getCount();
//           Logger.getLogger(commentsCollection.toString());
        }
        catch(Exception e)
        {
             Logger.getLogger(MongoConnector.class.getName()).log(Level.SEVERE, "Something wrong at getCollectionCount", e);
        }
        return count;
    }
    /**
     * This function is used to create the comment document for insertion into mongoDB. 
     * @param jsonString - The string representation of the JSON
     * @param principal - The current user who is inserting the document
     * @param originalId - If this is a reply to an original comment, then the originalId is used to find this.
     * @return 
     */
    public DBObject createCommentDocument(String jsonString, String principal, String originalId)
    {
        DBObject commentDocument = null;
        if(originalId != null)
        {
            
        }
        return commentDocument;
    }
    /**
     * 
     * This function is used to add the document to the comments collection.
     * @return 
     */
    public boolean addToCollection(DBObject comment)
    {
        try
        {
            commentsCollection.insert(comment);
            return true;
        }
        catch(Exception ex)
        {
            Logger.getLogger(MongoConnector.class.getName()).log(Level.SEVERE, "Something wrong at addToCollection", ex);
            return false;
        }
    }
}
