/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Websockets;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 *
 * @author deepak
 */
@ServerEndpoint("/wsmalendpoint")
public class wsMalEndPoint {
    private static Set<Session> peers = Collections.synchronizedSet(new HashSet<Session>());
    @OnOpen
    public void onOpen (Session peer) {
        peers.add(peer);
    }

    @OnClose
    public void onClose (Session peer) {
        peers.remove(peer);
    }
    @OnMessage
    public String onMessage(String messageString, Session peer) 
    {
        broadcast(messageString,peer);
        return "[Server] ok - I broadcasted the message: "+messageString;
    }

    private void broadcast(String messageString, Session peer) {
        try {
            for(Session thispeer:peers)
            {
                if(!thispeer.equals(peer))
                    thispeer.getBasicRemote().sendText("[Server - Broadcasting] " + messageString);
            }
            
        } catch (IOException ex) {
            Logger.getLogger(wsMalEndPoint.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
