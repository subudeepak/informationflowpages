/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Websockets;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 *
 * @author deepak
 */
@ServerEndpoint("/comments")
public class CommentsEndpoint {

    private static Set<Session> peers = Collections.synchronizedSet(new HashSet<Session>());
    @OnOpen
    public void onOpen (Session peer) {
        peers.add(peer);
    }

    @OnClose
    public void onClose (Session peer) {
        peers.remove(peer);
    }
    @OnMessage
    public String onMessage(String messageString, Session peer) 
    {
        return "Thanks for the comment"+messageString;      
    }
    
    
}
