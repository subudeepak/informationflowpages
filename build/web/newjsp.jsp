<%-- 
    Document   : newjsp
    Created on : Feb 5, 2014, 6:18:03 PM
    Author     : deepak
--%>
<%@page import="DatabaseConnector.MongoConnector" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <%
            
((HttpServletResponse)response).setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); 
%>
    </head>
    <body>
        <h1>Hello World!<%=request.getUserPrincipal()%></h1>
        <%= MongoConnector.mongoConnector.getCollectionCount()%>
    </body>
</html>
