/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
var wsUri = "wss://" + document.location.host + "/wsmalendpoint"; //document.location.host
try
{
    var websocket = new WebSocket(wsUri);
}
catch(err)
{
    onError(err);
}
websocket.onerror = function(evt) { onError(evt); };
websocket.onmessage = function(evt) { onMessage(evt); };
function onError(evt) {
    writeToScreen('<span style="color: red;">ERROR:</span> ' + evt.data);   
}
websocket.onopen = function(evt) { onOpen(evt); };
function writeToScreen(message) {
    var output = document.getElementById("output");
    output.innerHTML += '<div class="ink-alert basic info"><p>'+message+'</p></div>';
}
function onOpen() {
    writeToScreen("Connected to " + wsUri);
    websocket.send('hello');
    
}
function onMessage(evt) {
    writeToScreen("Received from server: " + evt.data);
}
function closeConnection()
{
    websocket.close();
}
function sendMessage()
{
    websocket.send(document.getElementById('message').value);
}
document.addEventListener('DOMContentReady', function () { document.getElementById('sendMsg').addEventListener('click', websocket.send(document.getElementById('message').value)); });
